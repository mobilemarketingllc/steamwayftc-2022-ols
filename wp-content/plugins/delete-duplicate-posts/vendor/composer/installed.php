<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '5977d44b5bdb5c3ebf7afa6fda7f4846decd8c0e',
        'name' => '__root__',
        'dev' => false,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '5977d44b5bdb5c3ebf7afa6fda7f4846decd8c0e',
            'dev_requirement' => false,
        ),
        'collizo4sky/persist-admin-notices-dismissal' => array(
            'pretty_version' => '1.4.4',
            'version' => '1.4.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../collizo4sky/persist-admin-notices-dismissal',
            'aliases' => array(),
            'reference' => '900739eb6b0ec0210465f5983a6d4e0e420289e4',
            'dev_requirement' => false,
        ),
    ),
);
